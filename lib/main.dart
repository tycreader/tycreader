import 'package:flutter/material.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:tycreader/screens/fromimages/tyc_from_images_page.dart';
import 'package:tycreader/screens/fromurl/tyc_from_url_page.dart';
import 'package:tycreader/screens/home/home_page.dart';
import 'package:tycreader/screens/navigation.dart';

void main() {
  Crashlytics.instance.enableInDevMode = true;
  FlutterError.onError = Crashlytics.instance.recordFlutterError;

  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TyCReader',
      initialRoute: Navigation.HOME_ROUTE,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      //home: HomePage(title: 'TyCReader'),
      routes: getRoutes(),
    );
  }

  Map<String, Widget Function(BuildContext context)> getRoutes(){
    return {
      Navigation.HOME_ROUTE: (context) => HomePage(title: 'TyCReader'),
      Navigation.TYC_FROM_URL_PAGE_ROUTE: (context) => TycFromUrlPage(),
      Navigation.TYC_FROM_IMAGES_PAGE_ROUTE: (context) => TycFromImagesPage(),
    };
  }
}


/*class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  File pickedImage;
  bool isImageLoaded = false;

  Future pickImage() async{
    var tempStore = await ImagePicker().getImage(source: ImageSource.gallery);

    setState(() {
      pickedImage = File(tempStore.path);
      isImageLoaded = true;
    });
  }

  Future readText() async {
    FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(pickedImage);
    TextRecognizer textReader = FirebaseVision.instance.textRecognizer();
    VisionText textReaded = await textReader.processImage(ourImage);

    print(textReaded.text);
    for(TextBlock block in textReaded.blocks){
      print(block.text);
      for(TextLine line in block.lines){
        print(line.text);
        for(TextElement word in line.elements)
          print(word.text);
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            RaisedButton(
              child: Text('Cargar TyC desde URL'),
              onPressed: (){},
            ),
            SizedBox(height: 10.0,),
            RaisedButton(
              child: Text('Cargar TyC desde fotos'),
              onPressed: (){},
            ),

            isImageLoaded ? Center(
            child: Container(
                height: 300.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: FileImage(pickedImage), fit: BoxFit.fitHeight)
                ),
                child: Image.file(pickedImage, fit:  BoxFit.fill,),
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 0, sigmaY: 0),
                  child: Container(
                    color: Colors.black.withOpacity(0),
                  ),
                ),
              ),
            ): Container(),
            SizedBox(height: 10.0,),
            RaisedButton(
              child: Text('Pick an image'),
              onPressed: pickImage,
            ),
            SizedBox(height: 10.0,),
            RaisedButton(
              child: Text('Read Text'),
              onPressed: readText,
            ),
          ],
        ),
      ),
    );
  }
}*/
