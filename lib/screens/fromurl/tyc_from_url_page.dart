import 'package:flutter/material.dart';
import 'package:tycreader/styles/pallete.dart';

class TycFromUrlPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.white,
      alignment: Alignment.center,
      child: Text("TycFromUrlPage", style: TextStyle( fontSize: 10, color: Palette.mainColor),),
    );
  }
}
