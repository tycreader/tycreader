import 'package:flutter/material.dart';
import 'package:tycreader/styles/pallete.dart';

class TycFromImagesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.white,
      alignment: Alignment.center,
      child: Text("TycFromImagesPage", style: TextStyle( fontSize: 10, color: Palette.mainColor),),
    );
  }
}
