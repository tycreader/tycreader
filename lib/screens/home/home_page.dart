import 'package:flutter/material.dart';
import 'package:tycreader/screens/navigation.dart';
import 'package:tycreader/styles/pallete.dart';

class HomePage extends StatelessWidget {
  final String title;

  HomePage({this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Palette.mainColor,
      ),

      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          RaisedButton(
            child: Text('Cargar TyC desde URL'),
            onPressed: ()=>Navigation.goToTyCFromURL(context),
          ),
          SizedBox(height: 10.0,),
          RaisedButton(
            child: Text('Cargar TyC desde fotos'),
            onPressed: ()=>Navigation.goToTyCFromImages(context),
          ),
        ],
      ),
    );
  }
}
