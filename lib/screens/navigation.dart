
import 'package:flutter/material.dart';

class Navigation{
  static const String HOME_ROUTE = "/homepage";
  static const String TYC_FROM_URL_PAGE_ROUTE = "/tycfromurlpage";
  static const String TYC_FROM_IMAGES_PAGE_ROUTE = "/tycfromimagepage";

  static void goToHome(BuildContext context){
    Navigator.pushNamed(context, HOME_ROUTE);
  }

  static void goToTyCFromURL(BuildContext context){
    Navigator.pushNamed(context, TYC_FROM_URL_PAGE_ROUTE);
  }

  static void goToTyCFromImages(BuildContext context){
    Navigator.pushNamed(context, TYC_FROM_IMAGES_PAGE_ROUTE);
  }

  static void goBackToHome(BuildContext context){
    if(Navigator.canPop(context)){
      Navigator.popUntil(context, (route) => route.isFirst);
    }
  }

  static void goBack(BuildContext context){
    if(Navigator.canPop(context)){
        Navigator.pop(context);
    }
  }
  /*static navigateToNamedRoute(BuildContext context, String namedRoute){
    switch(namedRoute){
      case HOME_ROUTE:
      Navigator.pushNamed(context, routeName)
      break;
    }
  }*/
}